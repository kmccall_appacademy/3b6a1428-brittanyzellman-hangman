require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(options = { guesser: "", referee: "" })
    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    word_length = @referee.pick_secret_word
    @board = []
    word_length.times { @board << "_" }
    @guesser.register_secret_length(word_length)
  end

  def update_board(arr, ch)
    arr.each { |i| @board[i] = ch }
  end

  def take_turn
    guess = @guesser.guess
    indices = @referee.check_guess(guess)
    update_board(indices, guess)
    @guesser.handle_response(guess, @board)
  end


end

class HumanPlayer
  attr_reader :guess, :dict, :guess, :word_length

  def initialize(dictionary)
    @dict = dictionary
    if dictionary.nil?
      File.foreach("lib/dictionary.txt") { |word| @dict << word.chomp }
    end
    @dict.length == 1 ? @word = dictionary[0] : @word = @dict[rand(@dict.length)]
  end

  def guess(board)
    # posible_matches = []
    # @dict.each { |w| posible_matches << w if w.length == @word_length }
    alpha = ('a'..'z').to_a
    alpha[rand(alpha.length)]
  end

  def register_secret_length(length)
    @word_length = length
  end

  def pick_secret_word
    i = rand(@dict.length)
    register_secret_length(@dict[i].length)
  end

  def check_guess(letter)
    indices = []
    @word.chars.each_with_index do |ch, i|
      indices << i if ch == letter.downcase
    end
    indices
  end

  def handle_response(letter, arr)
    check_guess(letter)
    if arr.any? { |el| el == "_" }
      take_turn
    else
      "Game over"
    end
  end

end

class ComputerPlayer
  attr_reader :dict, :word, :guess, :guesses, :word_length, :candidate_words

  def initialize(dictionary)
    @dict = dictionary
    if dictionary.nil?
      File.foreach("lib/dictionary.txt") { |word| @dict << word.chomp }
    end
    @dict.length == 1 ? @word = dictionary[0] : @word = @dict[rand(@dict.length)]
    @candidate_words = dictionary
    @guesses = []
  end

  def candidate_words(letter = nil, arr = nil)
    if letter == nil && arr == nil
      @candidate_words = @candidate_words.select { |w| w.length == @word_length }
    elsif !letter.nil? && arr.empty?
      @candidate_words = candidate_words.reject do |word|
        word.include?(letter)
      end
    else
      # debugger
      @candidate_words = @candidate_words.select do |word|
        word.chars[arr[0]] == letter
      end
      @candidate_words = @candidate_words.reject do |word|
        word.count(letter) > arr.length
      end
    end
    @candidate_words
  end

  def pick_secret_word
    i = rand(@dict.length)
    register_secret_length(@dict[i].length)
  end

  def register_secret_length(length)
    @word_length = length
  end

  def guess(board)
    alpha_hash = Hash.new(0)
    @candidate_words.each do |word|
      word.each_char { |char| alpha_hash[char] += 1 }
    end
    alpha = alpha_hash.sort_by { |k, v| v }
    i = 0
    # alpha is now a list of keys from greatest occurrance to least
    alpha = alpha.to_h.keys.reverse
    alpha.each do |letter|
      if board.any? { |ch| ch == letter }
        i += 1
      else
        return alpha[i]
      end
    end
  end

  def check_guess(letter)
    indices = []
    @word.chars.each_with_index do |ch, i|
      indices << i if ch == letter.downcase
    end
    indices
  end

  def handle_response(letter, arr)
    # arr = check_guess(letter)
    # if check_guess(letter) != []
    candidate_words(letter, arr)
    if arr.any? { |el| el == "_" }
      take_turn
    else
      "Game over"
    end
  end


end
